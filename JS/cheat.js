"use strict";
/**
 *@authors Danny Hoang & Noah Diplarakis
 *@version 2023-11-03 
 *js file used to create a cheat mode
 */


/**
 * @param {*} e 
 * function to create event listeners
 */
function setup(e) {
  document.body.addEventListener("keydown", function(e){
    let colorPick=document.querySelectorAll("select")[0].value;
    if (e.shiftKey && e.key == "C")
    showValues(colorPick);
  });
}
document.addEventListener("DOMContentLoaded", setup);

/**
 * function to show values for all the tds in the gameboard when you press shift+ C
 * @param {String} colorPick - color that the user has selected for the game.
 */
function showValues(colorPick){
let tds = document.querySelectorAll("td");
 //if its text content is empty for the first div then make it have content else no text content.
  if (tds[0].textContent) {
    Array.from(tds).forEach((td) => {
      td.textContent = "";
    });
  } else {
    Array.from(tds).forEach((td) => {
      let p = document.createElement("p");
      p.textContent = td.style.backgroundColor
      td.appendChild(p);
     
    });
    showDominants(colorPick);
  }
}
/**
 * This function takes in the color value of what the user selected it will append text content of the 
 * color that the user is trying to look for, for all dominant tiles of that selected color.
 * @param {String} colorPick - color that the user has selected for the game.
 */
function showDominants(colorPick){
  
  let dominants = correctTds(colorPick);
  Array.from(dominants)
  .forEach((dominantDiv) => {
   
    let p = document.createElement("p");
    p.textContent = colorPick;
    dominantDiv.appendChild(p);
  });
  console.log(dominants);
}
