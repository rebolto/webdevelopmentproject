"use strict";
/**
 * @Authors Danny Hoang & Noah Diplarakis
 * @version 2023-10-23
 *Javascript to run the game.
 */

function setup() {
  let message=document.getElementsByTagName("aside")[0];
  const submitbutton = document.getElementById("submitbutton");
  const startGame = document.getElementById("startgame");
  const LeaderboardNames = document.getElementById("playernames");
  const LeaderboardScores = document.getElementById("playerscores");
  //GAME

  appendLocalStorage(LeaderboardNames, LeaderboardScores);

  startGame.addEventListener("click", function drawTable() {
    //grab all the stuff from the dom to draw the table when user clicks start game

    let difficulty = document.querySelectorAll("input")[2].value;
    const chosenColor = document.querySelectorAll("select")[0].value;
    const tableContainer = document.getElementById("tableContainer");
    let tableSize = document.querySelectorAll("input")[1].value;
    const submitbutton = document.getElementById("submitbutton");

    //shows the submit button to let users submit their answer
    showSubmitGuessButton(submitbutton);
    createTableElements(tableContainer, tableSize);
    //grabs tds that were created
    const tds = document.querySelectorAll("td");
  
    colorTheTable(tds, difficulty, chosenColor);

    tableContainer.addEventListener("click",function(e){
     updateMessage(message,chosenColor);
    })
    message.textContent= `Searching for ${chosenColor} tiles! Your target is ${correctTds(chosenColor).length} tiles! ${0} selected!`;
  });

 
  startGame.addEventListener("click",function(){
    //initialising all form controls
    let playerinput=document.querySelectorAll("input")[0];
    let gameSizeInput=document.querySelectorAll("input")[1];
    let colorPick=document.querySelectorAll("select")[0];
    let difficultyinput=document.querySelectorAll("input")[2];
    //disabling all controls when start game button is clicked

    playerinput.disabled=true;
    gameSizeInput.disabled=true;
    colorPick.disabled=true;
    difficultyinput.disabled=true;
    startGame.disabled=true;
  })
  
  submitbutton.addEventListener("click",function(){


    //When submit guess button is clicked game controls are reenabled
    let startGame = document.getElementById("startgame");
    let playerinput=document.querySelectorAll("input")[0];
    let gameSizeInput=document.querySelectorAll("input")[1];
    let colorPick=document.querySelectorAll("select")[0];
    let difficultyinput=document.querySelectorAll("input")[2];

    hideSubmitGuessButton(submitbutton);
    playerinput.disabled=false;
    gameSizeInput.disabled=false;
    colorPick.disabled=false;
    difficultyinput.disabled=false;
    startGame.disabled=false;
  })
  
 


  submitbutton.addEventListener("click", function (e) {
    let difficulty = document.querySelectorAll("input")[2].value;
    let tableSize = document.querySelectorAll("input")[1].value;
    const userName = document.querySelectorAll("input")[0].value;
    const LeaderboardNames = document.getElementById("playernames");
    const LeaderboardScores = document.getElementById("playerscores");
    addPlayerToLeaderboard(tableSize,difficulty,LeaderboardNames,LeaderboardScores,userName);
  });

  submitbutton.addEventListener("click",function(e){
    let chosenColor=document.querySelectorAll("select")[0].value;
    updateMessagePercent(message,chosenColor);
  })
  
}
document.addEventListener("DOMContentLoaded", setup);
