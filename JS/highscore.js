"use strict";

/**
 * @Authors Danny Hoang & Noah Diplarakis
 * @version 2023-10-23
 * Javascript to implement the scoreboard.
 */

/**
 * function to create event listeners
 */
function setup(){
  let order = "descending";
  const LeaderboardNames = document.getElementById("playernames");
  const LeaderboardScores = document.getElementById("playerscores");
  clearbutton.addEventListener("click",function(e){
    clearLeaderboard(LeaderboardNames,LeaderboardScores);
  });

  const header = document.getElementById("highscorehead");
  header.addEventListener("click", function (e){
    order = changeScoreBoardOrder(order);
    appendLocalStorage(LeaderboardNames,LeaderboardScores,order)
  })
  } 
  /**
   * 
   * @param {String} order - A string value that will be changed
   *  depending on its current value "descending" or "ascending"
   * @returns {String} - the value it has swapped to based on its initial state,
   *  "descending" to "ascending" or "ascending" to "descending".
   */
  function changeScoreBoardOrder(order){
    {
      if(order == "descending"){
        order = "ascending";
        return order;
      }
      if(order == "ascending"){
        order = "descending";
        return order;
      }
   }
  };

/**
 * 
 * @param {Number} tableSize 
 * @param {Number} difficulty 
 * @param {HTMLElement} LeaderboardNames 
 * @param {HTMLElement} LeaderboardScores 
 * @param {String} userName 
 * function to add player to the leaderboard
 */
function addPlayerToLeaderboard(tableSize,difficulty,LeaderboardNames,LeaderboardScores,userName) {
  tableSize = parseInt(tableSize);
  difficulty = parseInt(difficulty);
  const userScore = getScore(numCorrect(),userSelected().length,tableSize,difficulty);
  cachePlayer(userScore, userName);
  appendLocalStorage(LeaderboardNames,LeaderboardScores);
}

/**
 * 
 * @param {HTMLElement} LeaderboardNames 
 * @param {HTMLElement} LeaderboardScores 
 * function to refresh sections and appends the players and scores
 */

function appendLocalStorage(LeaderboardNames, LeaderboardScores,order) {
  let players = JSON.parse(localStorage.getItem("Players")) || [];
  //two div templates to be cloned
  const playerNameTemplate = document.querySelector("#playerNameTemplate");
  const playerScoreTemplate = document.querySelector("#playerScoreTemplate");
 
  //clear divs before appending
    clearDivs(LeaderboardNames);
    clearDivs(LeaderboardScores);

  // Sort players by score in decscending order
 

  // Keep only the top 10 players (if more than 10 exist)
  players = players.slice(0, 10);


  if(order == "descending"){
    players.sort((a, b) => b.score - a.score);
  }
  if(order == "ascending") {
   players.sort((a, b) => a.score - b.score);
  }
  players.forEach((player) => {
    const playerNameClone = document.importNode(
      playerNameTemplate.content,
      true
    );
    const playerScoreClone = document.importNode(
      playerScoreTemplate.content,
      true
    );

    // Populate the template content with player data
    playerNameClone.querySelector("div").textContent = player.name;
    playerScoreClone.querySelector("div").textContent = player.score;

    // Append the populated templates to the sections
    LeaderboardNames.appendChild(playerNameClone);
    LeaderboardScores.appendChild(playerScoreClone);
  });
}
/**
 * 
 * @param {HTMLElement} section 
 * function  to clear the divs inside playername and scores
 */
function clearDivs(section) {
  const divs = section.querySelectorAll("div");
  divs.forEach((div) => {
    section.removeChild(div);
  });
}
/**
 * 
 * @param {Number} userScore 
 * @param {String} userName 
 * create a player
 */
function cachePlayer(userScore, userName) {
  const player = {
    name: userName,
    score: userScore,
  };
  
  // Retrieve the existing player list from localStorage or create an empty array if it doesn't exist
  let players = JSON.parse(localStorage.getItem("Players")) || [];
 
  // Add the new player to the list
  players.push(player);
  // Update the player list in localStorage
  localStorage.setItem("Players", JSON.stringify(players));
}


/**
 * 
 * @param {Number} numCorrect 
 * @param {Number} numSelected 
 * @param {Number} boardSize 
 * @param {Number} difficulty 
 * function to get score for highscore section
 * @returns score for the highscore
 */
function getScore(numCorrect, numSelected, boardSize, difficulty) {
  const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
  return Math.floor(percent * 100 * boardSize * (difficulty + 1));
}

// compare the users answer and compare the correct answers, returns a number of correct answers that the user has chosen 
//*warning user is able to select over the amount of correct answers on the board*
/**
 * Function to get the number of correct answers(td)
 * @returns the number of correct answers
 */
function numCorrect() {
  const chosenColor = document.querySelectorAll("select")[0].value;
  const correctTdsResult = correctTds(chosenColor);
  const userAnswers = userSelected();

  const userCorrectAnswers = correctTdsResult.filter((td) =>
    Array.from(userAnswers).includes(td)
  );

  return userCorrectAnswers.length;
}
/**
 * 
 * @param {String} chosenColor 
 * function that returns a nodelist of correct tds 
 */
function correctTds(chosenColor) {
  //the background color will return the string "RGB 0,0,0" were filtering out words and commas 
  //and keeping the numbers "0 0 0" for example. Will be put into a array to be compared on which rgb values is the highest.
  return Array.from(document.querySelectorAll("td")).filter((td) => {
    const backgroundColorString = td.style.backgroundColor;
    const rgbValues = backgroundColorString.match(/\d+/g).map(Number);

    if (
      chosenColor === "Red" &&
      rgbValues[0] >= rgbValues[1] &&
      rgbValues[0] >= rgbValues[2]
    ) {
      return true;
    } else if (
      chosenColor === "Green" &&
      rgbValues[1] >= rgbValues[0] &&
      rgbValues[1] >= rgbValues[2]
    ) {
      return true;
    } else if (
      chosenColor === "Blue" &&
      rgbValues[2] >= rgbValues[0] &&
      rgbValues[2] >= rgbValues[1]
    ) {
      return true;
    } else {
      return false;
    }
  });
}
/**
 * function 
 * @returns nodelist of selected tds
 */
//What the user selected returns a node list of selected answers
function userSelected() {
  const table = document.querySelectorAll("table")[0];
  return table.querySelectorAll("td.selected");
}


/**
 * 
 * @param {HTMLElement} LeaderboardNames 
 * @param {HTMLElement} LeaderboardScores 
 * function to clear the highscore leaderboard
 */
function clearLeaderboard(LeaderboardNames,LeaderboardScores){
  localStorage.clear();
  clearDivs(LeaderboardNames);
  clearDivs(LeaderboardScores);
}



document.addEventListener("DOMContentLoaded",setup);