"use strict";

/**
 * @Authors Danny Hoang & Noah Diplorakis
 * @version 2023-10-23
 *Javascript to implement the customizable gameboard.
 */

 /**
  * function to check the users submission
  */

/**
 * 
 * @param {HTMLElement} tableContainer 
 * @param {Number} tableSize 
 * function to generate a table
 */
function createTableElements( tableContainer, tableSize) {
  let tableExist = document.querySelector("table");
  if (tableExist) {
    tableExist.remove();
  }
  const table = document.createElement("table");

  //table size is created with a nested loop
  for (let row = 0; row < tableSize; row++) {
    let tr = document.createElement("tr");
    table.appendChild(tr);

    for (let column = 0; column < tableSize; column++) {
      let td = document.createElement("td");
      tr.appendChild(td);
    }
  }
  tableContainer.appendChild(table);
}
/**
 * 
 * @param {HTMLElement} submitbutton 
 * function to show the submit button when game is started
 */
function showSubmitGuessButton(submitbutton) {
  submitbutton.classList.remove("hidden");
}
/**
 * 
 * @param {HTMLElement} submitbutton 
 * function to show the submit button when game is ended
 */
function hideSubmitGuessButton(submitbutton) {
  submitbutton.classList.add("hidden");
}
/**
 * 
 * @param {HTMLElement} tds 
 * @param {Number} difficulty 
 * function to give td's randomized colors
 */
function colorTheTable(tds, difficulty) {
  Array.from(tds).forEach((td) => {
    td.style.backgroundColor = changeRgb(difficulty);
    td.addEventListener("click", selectedTd);

  });
}
/**
 * 
 * @param {*} e 
 * function to display a border if the targetted TD is selected
 */
function selectedTd(e){
  if(e.target.tagName==='TD'){
  if (e.target.classList == "selected"){
    e.target.classList.remove("selected");
  } else {
    e.target.classList.add("selected");
  }
}
}
/**
 * 
 * @param {HTMLElement} message 
 * @param {String} chosenColor 
 * function to update the selected message
 */
function updateMessage(message,chosenColor){
  let countSelectedTD=userSelected().length
  message.textContent= `Searching for ${chosenColor} tiles! Your target is ${correctTds(chosenColor).length} tiles! ${countSelectedTD} selected!`;
}
/**
 * 
 * @param {HTMLElement} message 
 * @param {String} chosenColor 
 * function used to display the total percentage for each game when submit guess button is clicked
 */
function updateMessagePercent(message,chosenColor){
  const numOfUserCorrectAnswers=numCorrect();
  const numOfUserSelectedTds=userSelected().length;
  const correctTDs=correctTds(chosenColor).length;
  let percentage;
  if(numOfUserSelectedTds>correctTDs){
    const percentForOneRightAnswer=100/correctTDs;
    const numTDsOverTarget=numOfUserSelectedTds-correctTDs;
    const percentagededuction=numTDsOverTarget*percentForOneRightAnswer;
    percentage=Math.round(
    Math.round((numOfUserCorrectAnswers/correctTDs)*100)-percentagededuction
                         );
    if(percentage<=0){
    percentage=0;
    }
    message.textContent=`You got ${percentage} percent! Next time dont select more than what is asked!`;
  }
  else{
     percentage=(Math.round(numOfUserCorrectAnswers/correctTDs*100));
    message.textContent=`You got ${percentage} percent!`;

  }
  

}