/**
 * @Authors Danny Hoang & Noah Diplarakis
 * @version 2023-11-01
 */

/**
 * holds the validation for input to turn on the start game button which has been initialised to disabled
 */
function setup(){
    let startgame=document.getElementById("startgame");
    let playernameInput=document.querySelectorAll("input")[0];
    let gameSizeInput=document.querySelectorAll("input")[1];
    let form=document.forms[0];
    playernameInput.addEventListener("input",function(){
        if(!(regexNamePattern().test(playernameInput.value))){
          playernameInput.setCustomValidity("Name must be alphanumeric and atleast 5 characters long");
          playernameInput.reportValidity();
        }
        else{
          playernameInput.setCustomValidity("");
        }
      });
      form.addEventListener("change",function(){
        if(form.checkValidity()){
            startgame.disabled=false;
        }
        else{
            startgame.disabled=true;
        }
      })
}
/**
 * function used to hold a regex containing the name validation
 * @returns a regex pattern containing what the name should be
 */
function regexNamePattern(){
    return /^[a-zA-Z0-9\s]{5,}$/;
}



document.addEventListener("DOMContentLoaded",setup);

