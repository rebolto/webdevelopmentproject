
"use strict";
/**
 * function to get a random number
 * @param {Number} range 
 * @returns randomized number
 */
//randomizer function for numbers between 0 - range
function randomNum(range) {
  return Math.floor(Math.random() * range);
}
/**
 * function to pull DOMElements and create event listeners
 */
function setup(){
  let selectValue=document.querySelectorAll("select")[0];
  let startbutton=document.getElementById("startgame");
  selectValue.addEventListener("change",function(e){
  startbutton.style.backgroundColor=selectValue.value;
  })
  
}
//randomized rgb values for each td.
/**
 * function to get a template rgb string
 * @param {Number} difficulty 
 * @returns template string of RGB for color
 */
function changeRgb(difficulty) {
  let baseColor = randomNum(256); 
  
  if (difficulty == 0) {

    return `rgb(${randomNum(256)},${randomNum(256)},${randomNum(256)})`;
  }

  if (difficulty == 1) {
    baseColor = randomNum(256 - 80);
    return `rgb(${baseColor + randomNum(80)},${baseColor + randomNum(80)},${baseColor + randomNum(80)})`;
  
  }

  if (difficulty == 2) {
      baseColor = randomNum(256 - 40);
      return `rgb(${baseColor + randomNum(40)},${baseColor + randomNum(40)},${baseColor + randomNum(40)})`;

  }

  if (difficulty == 3) {
    baseColor = randomNum(256 - 10);
    return `rgb(${baseColor + randomNum(10)},${baseColor + randomNum(10)},${baseColor + randomNum(10)})`;
  }
  // Return a default color if no condition matched
  return `rgb(${randomNum(256)},${randomNum(256)},${randomNum(256)})`;
}

document.addEventListener("DOMContentLoaded",setup);

