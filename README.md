Our Colour Guessing game is a project where you select tiles that you think have the most Dominant color. For example, if you choose to guess for green, you will select the tiles that you think have the most green in them. 

To Run this project, you must put in your name, which must be greater than 5 characters that can contain spaces. You must also put in the size of the game which ranges between a 3x3 and a 7x7 tile game, aswell as the color you'd like to guess ( RED,GREEN, OR BLUE). Finally, you must set a  difficulty which ranges from 0-3 getting harder to guess tiles progressively.


TO TURN ON CHEATMODE PRESS (Shift+C) at the same time!!!!
Cheatmode shows you the numbers of the rgb values in each tile, it displays the most dominant colour's name of the colour you are looking for, so when you are guessing, it makes it easier for you to answer.

If you turn on cheat mode (Shift+C) and see that the most dominant colour has the same number as another colour, it is considered a correct answer.